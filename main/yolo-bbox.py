import cv2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from config import cfg
import argparse
from tfflat.utils import mem_info


class Yolo:
    def getBbox(self, img_path):
        image = plt.imread(img_path)

        classes = None
        with open(cfg.coco_names, 'r') as f:
            classes = [line.strip() for line in f.readlines()]

        Width = image.shape[1]
        Height = image.shape[0]

        # read pre-trained model and config file
        net = cv2.dnn.readNet(cfg.yolov3_weights, cfg.yolov3_cfg)

        # create input blob
        # set input blob for the network
        net.setInput(cv2.dnn.blobFromImage(image, 0.00392,
                                           (416, 416), (0, 0, 0), True, crop=False))

        # run inference through the network
        # and gather predictions from output layers

        layer_names = net.getLayerNames()
        output_layers = [layer_names[i[0] - 1]
                         for i in net.getUnconnectedOutLayers()]
        outs = net.forward(output_layers)

        class_ids = []
        confidences = []
        boxes = []

        # create bounding box
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.1:
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])

        indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.1, 0.1)
        # print('indices length ' + str(len(indices)))

        # print('boxes ' + str(boxes))

        # print('first box ' + str(boxes[0]))
        peopleBoxes = []
        peopleConfidences = []
        # check if is people detection
        for i in indices:
            i = i[0]
            box = boxes[i]
            if class_ids[i] == 0:
                print('human box ' + str(box))
                peopleBoxes.append(box)
                peopleConfidences.append(confidences[i])
                # print('confidence ' + str(confidences))
                # label = str(classes[class_id])
                cv2.rectangle(image, (round(box[0]), round(box[1])), (round(
                    box[0]+box[2]), round(box[1]+box[3])), (0, 0, 0), 2)
                # cv2.putText(image, label, (round(
                #     box[0])-10, round(box[1])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
                break

        # plt.imshow(image)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        cv2.imwrite('/content/out/1.jpg', image)
        print('output image saved')
        if(len(peopleBoxes) == 0):
            return []
        return peopleBoxes, peopleConfidences


# yolo = Yolo()

if __name__ == '__main__':
    def parse_args():
        parser = argparse.ArgumentParser()
        parser.add_argument('--gpu', type=str, dest='gpu_ids')
        parser.add_argument('--test_epoch', type=str, dest='test_epoch')
        parser.add_argument('--img_path', type=str, dest='img_path')
        args = parser.parse_args()

        # test gpus
        if not args.gpu_ids:
            args.gpu_ids = str(np.argmin(mem_info()))

        if '-' in args.gpu_ids:
            gpus = args.gpu_ids.split('-')
            gpus[0] = 0 if not gpus[0].isdigit() else int(gpus[0])
            gpus[1] = len(mem_info()) if not gpus[1].isdigit() else int(
                gpus[1]) + 1
            args.gpu_ids = ','.join(map(lambda x: str(x), list(range(*gpus))))

        # assert args.test_epoch, 'Test epoch is required.'
        return args

    global args
    args = parse_args()
    img_path = args.img_path
    Yolo().getBbox(img_path)
